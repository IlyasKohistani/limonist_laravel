<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $hidden = [
        'created_at',
        'updated_at',
        'id',
        'user_id',
    ];

    /**
     * Get the user that belong to the post.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
