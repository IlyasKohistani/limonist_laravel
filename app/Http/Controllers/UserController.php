<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('JWT', ['except' => ['login', 'register', 'verify']]);
    }


    /**
     * 
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::all();
        return response()->json(['success' => true, 'data' => $users]);
    }

    /**
     * 
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy()
    {
        $user = auth()->user();
        if ($user->delete()) {
            auth()->logout();
            return response()->json(['success' => true, 'message' => 'You have successfully removed your account.']);
        } else {
            return response()->json(['success' => false, 'message' => 'An error occured while processing your request.']);
        }
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        // validate email and password
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4',
            'surname' => 'required|string|min:5',
            'username' => 'required|string|min:8',

        ]);
        
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()]);
        }
        $usernameExist = User::where('username', $request->username)->first();
        $user = User::find(auth()->user()->id);
        if (!empty($usernameExist) && $user->id != $usernameExist->id) {
            return response()->json(['success' => false, 'message' => ['username' => 'Username is taken!']]);
        }

        //update existing user
        $user->fname = $request->name;
        $user->username = $request->username;
        $user->lname = $request->surname;
        if ($user->save()) return response()->json(['success' => true, 'message' => 'Your information update successfully']);
        else return response()->json(['success' => false, 'message' => 'An error occured while processing your request!']);
    }
}
