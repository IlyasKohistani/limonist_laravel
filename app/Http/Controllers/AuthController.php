<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        // validate email and password
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|min:8',
            'password' => 'required|string|min:8',

        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()]);
        }

        // initialize the response
        $response = array(
            'success' => false,
            'isVerified' => false,
            'message' => '',
        );
        // generate token for 30 days or 1 hour
        $token_time = (isset($request->remember_me)) ? 43200 : 60;

        //get user data and validate login
        $user = User::where('email', $request->email)->orWhere('username', $request->email)->first();
        if (!$user) {
            $response['message'] = ['email' => 'Sorry, You have entered wrong email address.'];
            return response()->json($response);
        }
        if (!Hash::check($request->password, $user->password)) {
            $response['message'] = ['password' => 'Sorry, You have entered wrong password.'];
            return response()->json($response);
        }

        //login credentials
        $credentials = array('email' => $user->email, 'password' => $request->password);


        // login attempt
        if (!$token = auth()->setTTL($token_time)->attempt($credentials)) {
            $response['message'] = 'Email Or Password is invalid.' . json_encode($credentials);
            return response()->json($response);
        }

        // if login is successful then send token with response
        $response['success'] = true;
        $response['access_token'] = $token;
        $response['message'] = 'You have successfully logged in.';
        $response['fname'] = $user->fname;
        $response['lname'] = $user->lname;
        $response['username'] = $user->username;
        $response['email'] = $user->email;
        return $this->respondWithToken($response);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(['success' => true, 'data' => auth()->user()]);
    }

    /**
     * Register User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',

        ]);
        if ($validator->fails()) {
            return response()->json(['success' => false, 'message' => $validator->errors()]);
        }

        $data = array(
            'email' => $request->email,
            'password' => Hash::make($request->password),
        );

        if (DB::table('users')->insert($data))
            return $this->login($request);
        
        return response()->json(['success' => false, 'message' => 'An error occured while processing your request.'], 401);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();
        return response()->json(['success' => true, 'message' => 'You have successfully logged out.']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(['success' => true, 'access_token' => auth()->refresh()]);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($response)
    {
        $response['token_type'] = 'bearer';
        $response['expires_in'] = auth()->factory()->getTTL() * 60;
        return response()->json($response);
    }
}
