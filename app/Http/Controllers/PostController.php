<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Builder;

class PostController extends Controller
{

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        //this middleware does not give us details on errors
        // $this->middleware('auth:api', ['except' => ['login', 'register']]);
        //JWT middleware authenticate route and gives us good details on errors
        $this->middleware('JWT');
    }


    /**
     * Display a listing of the questions.
     *
     * @return \Illuminate\Http\Response
     */
    public function posts(Request $request)
    {

        $post = Post::all();
        return response()->json(['success' => true, 'data' => $post]);
    }
}
