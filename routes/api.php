<?php

use App\Http\Controllers\AnswerController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Start of auth routes
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('register', [AuthController::class, 'register']);
    Route::post('me', [AuthController::class, 'me']);
});
//End of auth routes

//Start of post routes
Route::group([

    'middleware' => 'api',
    'prefix' => 'post'

], function ($router) {

    Route::get('', [PostController::class, 'posts']);
});
//End of post routes

//Start of post routes
Route::group([

    'middleware' => 'api',
    'prefix' => 'user'

], function ($router) {

    Route::get('', [UserController::class, 'index']);
    Route::post('', [UserController::class, 'update']);
    Route::delete('', [UserController::class, 'destroy']);
});
//End of post routes


//fallback method if routes fail
Route::fallback(function () {
    return response()->json(['success' => false, 'message' => 'You have come to the wrong link(route).'], 404);
});
