<?php

namespace Database\Factories;

use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => 1,
            'branch' => Str::random(10),
            'report' => rand(1, 1000),
            'cash' => rand(1, 1000),
            'credit' => rand(1, 1000),
            'debit' => rand(1, 1000),
            'dovis' => rand(1, 1000),
            'mobile' => rand(1, 1000),
            'expense' => rand(1, 1000),
            'difference' => rand(1, 1000),
        ];
    }
}
